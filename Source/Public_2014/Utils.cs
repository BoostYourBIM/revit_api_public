﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Events;
using Autodesk.Revit.UI.Events;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.ApplicationServices;

namespace Public_2014
{
    public static class Utils
    {
    	public static double RadiansToDegrees(Double radians)
        {
            return (radians * 180 / Math.PI);
        }

        public static double DegreesToRadians(Double degrees)
        {
            return (degrees * Math.PI / 180);
        }	
        
        public static bool setParamIfExists(Element e, string paramName, string value)
        {
            Parameter p = e.get_Parameter(paramName);
            if (p == null)
                return false;
            return setParameter(p, value);
        }

        public static bool setParamIfExists(Element e, string paramName, int value)
        {
            Parameter p = e.get_Parameter(paramName);
            if (p == null)
                return false;
            return setParameter(p, value.ToString());
        }

        public static bool setParamIfExists(Element e, string paramName, double value)
        {
            Parameter p = e.get_Parameter(paramName);
            if (p == null)
                return false;
            return setParameter(p, value.ToString());
        }
		
		//Create a generic filtered selection.
		public class GenericSelectionFilter : ISelectionFilter
		{
			static string CategoryName = "";
			public GenericSelectionFilter (string catName)
			{
				CategoryName = catName;
			}		
			public bool AllowElement(Element element)
			{
				if (element.Category.Name == CategoryName)
				{
					return true;
				}
				return false;
			}
			public bool AllowReference(Reference refer, XYZ point)
			{
				return false;
			}
		}
        // useful when setting parameters from data read from an external source (Excel, txt, etc)
        // where all data will come in as a string and then must be converted to the proper type based on the parameter's StorageType
        public static bool setParameter(Parameter p, string s)
        {
            if (p.StorageType == StorageType.String)
            {
                try
                {
                    p.Set(s);
                }
                catch (Autodesk.Revit.Exceptions.InvalidOperationException)
                {
                    return false;
                }
            }
            else if (p.StorageType == StorageType.Integer)
            {
                int number;
                bool result = Int32.TryParse(s, out number);
                if (result)
                {
                    try
                    {
                        p.Set(number);
                    }
                    catch (Autodesk.Revit.Exceptions.InvalidOperationException)
                    {
                        return false;
                    }
                }
                else
                    TaskDialog.Show("Error", "Parameter " + p.Definition.Name + " requires an integer. Invalid input: " + s);
            }
            else if (p.StorageType == StorageType.Double)
            {
                double number;
                bool result = Double.TryParse(s, out number);
                if (result)
                {
                    try
                    {
                        p.Set(number);
                    }
                    catch (Autodesk.Revit.Exceptions.InvalidOperationException)
                    {
                        return false;
                    }
                }
                else
                    TaskDialog.Show("Error", "Parameter " + p.Definition.Name + " requires an double. Invalid input: " + s);
            }
            else if (p.StorageType == StorageType.ElementId)
            {
            	throw new NotImplementedException();
            }
            return true;
        }
        
        //Strips any illegal char. from string to all storage in the Windows filesystem
        public static string MakeValidFileName( string name )
		{
			string invalidChars = System.Text.RegularExpressions.Regex.Escape( new string( System.IO.Path.GetInvalidFileNameChars() ) );
			string invalidReStr = string.Format( @"([{0}]*\.+$)|([{0}]+)", invalidChars );
			return System.Text.RegularExpressions.Regex.Replace( name, invalidReStr, "_" );
		}
    }
}
