﻿/*
 * Revit Macros
 * Created By: MattBeNimble
 * Date: 10/10/2013
 */

using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using System;

namespace Public_2014
{
	public partial class ThisApplication
	{
		//Macro To export all schedules to the users myDocuments folder. This will export as CSV.
		//All ViewNames will be renamed to be valid windows names.
		public void MN_ExportAllSchedules()
		{
			UIDocument uidoc = this.ActiveUIDocument;
			Document doc = this.ActiveUIDocument.Document;
			
			//Get the RVT name and remove the extension
			string docName = uidoc.Document.Title.ToString();
			string title = docName.Replace(".rvt","");
			
			//Set Folder to MyDocs > RVT_Schedules > DocTitle
			String folderName = Environment.GetFolderPath(Environment.SpecialFolder.Personal).ToString() +"\\RVT_Schedules\\" + title + "\\";
			 
			//Set Filter to Schedules and set options
			FilteredElementCollector collection = new FilteredElementCollector(doc).OfClass(typeof(ViewSchedule));
			ViewScheduleExportOptions opt = new ViewScheduleExportOptions();
			
			//See If folder exists, delete existing exports or create folders
			if(Directory.Exists(folderName))
			{
				//For Each file remove from directory
				Array.ForEach(Directory.GetFiles(folderName), File.Delete);
			}
			else
			{
				//Create Named folder from Variable above
				Directory.CreateDirectory(folderName);
			}
			
			//Iterate through schedules and export to <Schedulename>.txt
			foreach( ViewSchedule vs in collection )
			{
				string viewName = Public_2014.Utils.MakeValidFileName(vs.Name);
				//Try Loop to avoid any export exceptions
				if(!vs.Name.Contains("<"))
			   try
				{
					//For Each Schedule - Export to individual CSV format. - No Options added
					vs.Export(folderName, viewName + ".csv", opt );
				}
				catch
				{
					//If there is an exception, throw a dialog.
					TaskDialog.Show("Error", vs.Name);
				}
			}
			
			//When Complete, let the user know.
			TaskDialog td = new TaskDialog("Schedule Exporter");
			td.MainContent = "All of the Revit schedules in project:" + Environment.NewLine +
			docName + Environment.NewLine + Environment.NewLine +
			"Have been exported to: " + Environment.NewLine +
			folderName ;
			td.MainInstruction = "Schedules have been exported.";
			 
			//Display form
			td.Show();
		}
		
		//Export all Isometric Views to Navisworks NWC
		public void MN_ExportNavisViews(){
		
			UIDocument uidoc = this.ActiveUIDocument;
			Document doc = this.ActiveUIDocument.Document;
			String folderName = Environment.GetFolderPath(Environment.SpecialFolder.Personal).ToString() +"\\RVT_Navis\\";
		
			string viewName = String.Empty;
			string fileName = String.Empty;
			
			if(Directory.Exists(folderName))
			{
				//For Each file remove from directory
				Array.ForEach(Directory.GetFiles(folderName), File.Delete);
			}
			else
			{
				//Create Named folder from Variable above
				Directory.CreateDirectory(folderName);
			}
		
			//Set Filter to 3DViews and set options
			FilteredElementCollector collection = new FilteredElementCollector(doc).OfClass(typeof(View3D));
			
			if( OptionalFunctionalityUtils.IsNavisworksExporterAvailable() == true){
				NavisworksExportOptions nwOpt = new NavisworksExportOptions();
					nwOpt.ExportScope = NavisworksExportScope.View;
					nwOpt.ConvertElementProperties = true;
					nwOpt.ExportParts = true;
				
				foreach (View3D view in collection) {
					if(view.IsTemplate == false && view.IsPerspective == false){
						viewName += view.Name + "[" + view.Id + "]" + Environment.NewLine;
						nwOpt.ViewId = view.Id;
						fileName = Public_2014.Utils.MakeValidFileName(view.Name);
						
						doc.Export(folderName,fileName,nwOpt);
					}
				}
				TaskDialog.Show("Views", viewName);
			}
		}
	
		//Allow Selection of Pipe Insulation Across Pipe Runs
		public void SelectPipeInsulation(){
		
			UIDocument uidoc = this.ActiveUIDocument;
			Document doc = this.ActiveUIDocument.Document;
			
			ISelectionFilter pipeFilter = new PipeSelectionFilter();
			IList<Reference> pipeRef = uidoc.Selection.PickObjects(ObjectType.Element, pipeFilter);
			//IList<ElementId> insulId = new List<ElementId>();
			SelElementSet selSet = SelElementSet.Create();
			
			using (Transaction t = new Transaction(doc,"Selecting Pipe Insulations"))
			{
				t.Start();
				
				foreach(Reference reference in pipeRef)
				{	
					Element e = doc.GetElement(reference.ElementId);
					//insulId.Add(reference.ElementId);
					selSet.Add(e);
				}
				
				uidoc.Selection.Elements = selSet;
				uidoc.RefreshActiveView();
				
				t.Commit();
			}
		}
		public class PipeSelectionFilter : ISelectionFilter
		{
			public bool AllowElement(Element element)
			{
				Autodesk.Revit.DB.Plumbing.PipeInsulation pipe = element as Autodesk.Revit.DB.Plumbing.PipeInsulation;
				
				if ( pipe == null)
					{return false;}
				return true;		
			}
			public bool AllowReference(Reference refer, XYZ point)
			{
				return false;
			}
		}
	
	}

}
