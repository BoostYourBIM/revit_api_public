﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Events;
using Autodesk.Revit.UI.Events;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.ApplicationServices;

namespace Public_2014
{
	public partial class ThisApplication
	{
		
		public void BoostYourBIMTest()
		{
			TaskDialog.Show("Hi","Boost Your BIM test");
		}
	
		// sample showing use of the Utils class
		public void SetParameterSample()
		{
			Document doc = this.ActiveUIDocument.Document;
			UIDocument uidoc = this.ActiveUIDocument;
			Element e = doc.GetElement(uidoc.Selection.PickObject(ObjectType.Element,"Select an element"));

			using (Transaction t = new Transaction(doc, "Set parameter"))
			{
				t.Start();
				if (Utils.setParamIfExists(e, "Comments", "Current time is " + DateTime.Now))
					TaskDialog.Show("Info","Parameter 'Comments' set.");
				else
					TaskDialog.Show("Info","Parameter 'Comments' could not be set.");
	
				if (Utils.setParamIfExists(e, "Unconnected Height", 20.5))
					TaskDialog.Show("Info","Parameter 'Unconnected Height' set.");
				else
					TaskDialog.Show("Info","Parameter 'Unconnected Height' could not be set.");
				t.Commit();
			}
		}
	}
}
