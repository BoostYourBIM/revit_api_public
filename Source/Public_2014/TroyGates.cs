﻿/*
 * Revit Macros
 * Created By: TroyGates
 * Date: 10/10/2013
 */

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Architecture;
using Autodesk.Revit.DB.Mechanical;
using Autodesk.Revit.DB.Lighting;
using Autodesk.Revit.DB.Plumbing;
using Autodesk.Revit.DB.Events;
using Autodesk.Revit.UI.Events;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.ApplicationServices;

namespace Public_2014
{
	public partial class ThisApplication
	{

		// Description: Delete all sheets and views in the model except the current view
		public void DeleteAllSheetsAndViews()
		{
			// setup uidoc and doc for accessing the Revit UI (uidoc) and the Model (doc)
			UIDocument uidoc = this.ActiveUIDocument;
			Document doc = uidoc.Document;
			
			// get all the elements in the model database
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			// filter out all elements except Views
			ICollection<Element> collection = collector.OfClass(typeof(View)).ToElements();
			
			// create a transaction
			using(Transaction t = new Transaction(doc, "Delete Views"))
			{
				// start the transaction
				t.Start();
				
				// add a counter to count the views/sheets deleted
				int x = 0;
				
				// loop through each view in the model
				foreach (Element e in collection)
				{			    	
					try 
					{	
						View view = e as View;
						
						// all views/sheets are deleted and increment counter by 1
						doc.Delete(e.Id);
						x += 1;
					}
					catch
					{
					}
				}
				// finalize the transaction
				t.Commit();
				// show message with number of views/sheets deleted
				TaskDialog.Show("DeleteSheets", "Views & Sheets Deleted: " + x.ToString());
			}
		}	
		
		// Description: Delete all sheets and all views except for floor and ceiling plans
		public void DeleteSheetsAndViews()
		{
			// setup uidoc and doc for accessing the Revit UI (uidoc) and the Model (doc)
			UIDocument uidoc = this.ActiveUIDocument;
			Document doc = uidoc.Document;
			
			// get all the elements in the model database
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			// filter out all elements except Views
			ICollection<Element> collection = collector.OfClass(typeof(View)).ToElements();
			
			// create a transaction
			using(Transaction t = new Transaction(doc, "Delete Views"))
			{
				// start the transaction
				t.Start();
				
				// add a counter to count the views/sheets deleted
				int x = 0;
				
				// loop through each view in the model
				foreach (Element e in collection)
				{			    	
					try 
					{	
						View view = e as View;
						
						// determine what type of view it is
						switch(view.ViewType)
						{
							// if view is a floor plan, don't delete
							case ViewType.FloorPlan:
								break;
							// if view is a ceiling plan, don't delete
							case ViewType.CeilingPlan:
								break;
							// all other views/sheets can be deleted and increment counter by 1
							default:
								doc.Delete(e.Id);
								x += 1;
								break;
						}
					}
					catch
					{
					}
				}
				// finalize the transaction
				t.Commit();
				// show message with number of views/sheets deleted
				TaskDialog.Show("DeleteSheets", "Views & Sheets Deleted: " + x.ToString());
			}
		}		
		
		// Description: Delete all views except for floor and ceiling plans
		public void DeleteViews()
		{
			// setup uidoc and doc for accessing the Revit UI (uidoc) and the Model (doc)
			UIDocument uidoc = this.ActiveUIDocument;
			Document doc = uidoc.Document;
			
			// get all the elements in the model database
			FilteredElementCollector collector = new FilteredElementCollector(doc);
			// filter out all elements except Views
			ICollection<Element> collection = collector.OfClass(typeof(View)).ToElements();
			
			// create a transaction
			using(Transaction t = new Transaction(doc, "Delete Views"))
			{
				// start the transaction
				t.Start();
				
				// add a counter to count the views deleted
				int x = 0;
				
				// loop through each view in the model
				foreach (Element e in collection)
				{			    	
					try 
					{	
						View view = e as View;
						
						// determine what type of view it is
						switch(view.ViewType)
						{
							// if view is a floor plan, don't delete
							case ViewType.FloorPlan:
								break;
							// if view is a ceiling plan, don't delete
							case ViewType.CeilingPlan:
								break;
							// if view is a sheet, don't delete
							case ViewType.DrawingSheet:
								break;
							// all other views can be deleted and increment counter by 1
							default:
								doc.Delete(e.Id);
								x += 1;
								break;
						}
					}
					catch
					{
					}
				}
				// finalize the transaction
				t.Commit();
				
				// show message with number of views/sheets deleted
				TaskDialog.Show("DeleteSheets", "Views Deleted: " + x.ToString());
			}
		}
		
		// Description: Export all sheets as DWG with bound views and using shared coordinates
		public void ExportSheetsToDWG()
		{
			// setup uidoc and doc for accessing the Revit UI (uidoc) and the Model (doc)
			UIDocument uidoc = this.ActiveUIDocument;
			Document doc = uidoc.Document;
			
			// get all the elements in the model database
		    FilteredElementCollector collector = new FilteredElementCollector(doc);
			// filter out all elements except Views
		    ICollection<Element> collection = collector.OfClass(typeof(ViewSheet)).ToElements();
		    
		    // create a transaction
		    using(Transaction t = new Transaction(doc, "Export Sheets"))
		    {
				// start the transaction
			    t.Start();
			    
			    // create a list to hold the sheets
			    List<ElementId> sheetsToDWG = new List<ElementId>();
			    
			    // create DWG export options
			    DWGExportOptions dwgOptions = new DWGExportOptions();
			    dwgOptions.MergedViews = true;
			    dwgOptions.SharedCoords = true;
			    
				// add a counter to count the sheets exported
			    int x = 0;
			 
			    // loop through each view in the model
			    foreach (Element e in collection)
			    {			    	
			    	try 
			    	{	
						ViewSheet viewsheet = e as ViewSheet;
						
						// only add sheets to list
						if (viewsheet.IsPlaceholder == false)
						{
							sheetsToDWG.Add(e.Id);
							x += 1;
						}
			    	}
			    	catch
			    	{
			    	}
			    }

			    string path = "";
			    string file = "";
			    
		    	// get the current date and time
		    	DateTime dtnow = DateTime.Now;
				string dt = string.Format("{0:yyyyMMdd HHmm}", dtnow);
			    
			    if (doc.PathName != "")
			    {
			    	// use model path + date and time
			    	path = Path.GetDirectoryName(doc.PathName) + "\\" + dt;			    	
			    }
			    else
			    {
			    	// model has not been saved
			    	// use C:\DWG_Export + date and time
			    	path = "C:\\DWG_Export\\" + dt;
					file = "NONAME";
			    }
			    
		    	// create folder
		    	Directory.CreateDirectory(path);
		    	
		    	// export
	    		doc.Export(path, file, sheetsToDWG, dwgOptions);
	    		
			    TaskDialog.Show("Export Sheets to DWG", x + " sheets exported to:\n" + path);
		    }
		}
	
	}
}